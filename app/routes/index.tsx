import Header from "@components/Header";

export default function Index() {
  return (
    <section style={{ display: "grid", gridTemplateAreas: "'header header header' 'hero hero hero' '. about .' '. contact .' 'footer footer footer'", gridTemplateColumns: "1fr 2fr 1fr" }} >
      <Header />
      <section style={{ gridArea: "about" }}>
        <h1>
          Welcome to my website
        </h1>
      </section>

    </section>
  );
}
