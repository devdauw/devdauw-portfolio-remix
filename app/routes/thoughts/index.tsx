import { Link } from "@remix-run/react";

import Post from "@components/Post";
import Header from "~/shared/components/Header";

export default function Thoughts() {
  return(
    <body>
      <Header />
      <h1>Thoughts</h1>
      <h2>Thoughts by <Link to={{ pathname: "https://www.linkedin.com/in/juliendendauw/" }} target="_blank" prefetch="intent">Julien Dendauw</Link></h2>
      <Post>
        Test POST
      </Post>
    </body>
  )
}