import { Link } from '@remix-run/react';

import PostImage from '@components/Post/postImage';
import PostIntro from '@components/Post/postIntro';

type PostProps = React.ComponentPropsWithoutRef<'section'> & {

}

export default function Post({children}: PostProps) {
  return (
    <section>
      <PostImage />
      <PostIntro />
      <Link to="linkToCurrentPost" />
    </section>
  );
}