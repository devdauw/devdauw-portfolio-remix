type ButtonProps = React.ComponentPropsWithoutRef<'button'> & {
  size?: 'sm' | 'md' | 'lg';
  theme?: 'primary' | 'secondary' | 'tertiary';
  variant?: 'ghost' | 'solid' | 'outline' | 'link';
}

export default function Button({children, size, theme, variant, ...rest}: ButtonProps): JSX.Element {
  return(
    <button {...rest} >
      {children}
    </button>
  )
}