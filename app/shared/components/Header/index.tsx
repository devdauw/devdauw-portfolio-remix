import {
  Link,
  NavLink
} from '@remix-run/react'

function HomeAnchor() {
  return (
    <Link to = "/" style={{ display: 'inherit' }}>
      <img alt="jden" src="https://via.placeholder.com/50" style={{ paddingRight: "15px" }}  />
      <h1>juliendendauw</h1>
    </Link>
  );
}

export default function Header({...props}) {
  return(
    <header style={{ gridArea: "header" }}>
      <nav>
        <div style={{ display: "flex", alignItems: "center", justifyContent: "space-around" }}>
          <HomeAnchor />
          <div>
            <NavLink to="/#aboutTitle" style={{ paddingRight: "15px" }}>about</NavLink>
            <NavLink to={"/thoughts"} prefetch="intent" style={{ paddingRight: "15px" }}>thoughts</NavLink>
            <NavLink to="/#contactTitle">contact</NavLink>
          </div>
        </div>
      </nav>
    </header>
  );
}
